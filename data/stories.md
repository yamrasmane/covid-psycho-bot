## say goodbye
* goodbye
  - utter_goodbye
* greet
  - utter_greet
## bot challenge
* bot_challenge
  - utter_iamabot

## survey happy path
* affirm
    - health_form
    - form{"name": "health_form"}
    - form{"name": null}
* thankyou
    - utter_no_worries
    - utter_goodbye

## survey stop
* affirm
    - health_form
    - form{"name": "health_form"}
* out_of_scope
    - utter_ask_continue
* deny
    - action_deactivate_form
    - form{"name": null}
    - utter_goodbye

## survey continue
* affirm
    - health_form
    - form{"name": "health_form"}
* out_of_scope
    - utter_ask_continue
* affirm
    - health_form
    - form{"name": null}

## no survey
* deny
    - utter_goodbye