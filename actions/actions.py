from typing import Any, Text, Dict, List, Union

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, SlotSet

class HealthForm(FormAction):

    def name(self):
        return "health_form"

    @staticmethod
    def required_slots(tracker):
        part1 = ["confirm_psycho",  "civilite","age", "niveau", "matrimoniale", "nbenfant", "habitation", "situation", "situationvie", 
        "famille", "connaissance_covid"]

        """ "confirm_connaissance_covid","""

        part2 = [ "lieu_info", "lieu_info2", 
             "q13", "q14", "q15", "q16", "q17", "q18", "q19", "q20", "q21", "q22", "q23", "q24", "q25", "q26", "q27", "q28"]
             
        part3 = ["q29", "q30", "q31"]

        part4 = ["q32", "q33", "q34", "q35"]

        part5 = ["q36", "q37", "q38", "q39", "q40", "q41", "q42", "q43", "q44"]

        part6 = ["q45", "q46", "q47", "q48", "q49", "q50", "q51", "q52", "q53", "q54", "q55", "q56", "q57"]

        part7 = ["q58", "q59", "q60", "q61", "q62", "q63", "q64", "q65", "q66", "q67", "q68", "q69"]

        part8 = [ "q70", "q71", "q72", "q73", "goal"]
             

        if tracker.get_slot('confirm_psycho') == True:
            a = tracker.get_slot('connaissance_covid')
            q31 = tracker.get_slot('q31')
            q35 = tracker.get_slot('q35')
            q57 = tracker.get_slot('q57')
            q69 = tracker.get_slot('q69')
            if a == 'oui':
                if q31 == 'oui':
                    if q35 == 'oui':
                        if q57 == 'oui':
                            if q69 == 'oui':
                                return ["boot_fin"]
                            if q69 == 'non':
                                return part8 + ["boot_fin"]
                            return ["consulter_medecin"] + part7
                        if q57 == 'non':
                            return ["boot_fin"]
                        return part5 + part6
                    if q35 == 'non':
                        return ["boot_fin"]
                    return part4
                if q31 == 'non':
                    return ["info_test1", "info_test2", "info_test3", "info_test_remercie", "boot_fin"]
                return part2 + part3
            if a == 'non':
                if q31 == 'oui':
                    if q35 == 'oui':
                        if q57 == 'oui':
                            if q69 == 'oui':
                                return ["boot_fin"]
                            if q69 == 'non':
                                return part8 + ["boot_fin"]
                            return ["consulter_medecin"] + part7
                        if q57 == 'non':
                            return ["boot_fin"]
                        return part5 + part6
                    if q35 == 'non':
                        return ["boot_fin"]
                    return part4
                if q31 == 'non':
                    return ["info_test1", "info_test2", "info_test3", "info_test_remercie", "boot_fin"]
                return  ["info_covid1", "info_covid2"] + part3
            
            
            return part1
        
        else:
            return ["confirm_psycho"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "confirm_psycho": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "civilite": [
                self.from_intent(intent="affirm", value=True),
                self.from_entity(entity="civilite"),
                self.from_intent(intent="deny", value="None"),
            ],
            "age": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_entity(entity="age"),
                self.from_text(intent="deny"),
            ],
            "niveau": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_entity(entity="niveau"),
                self.from_text(intent="deny"),
            ],
            "matrimoniale": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_entity(entity="matrimoniale"),
                self.from_text(intent="deny"),
            ],
            "nbenfant": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_entity(entity="nbenfant"),
                self.from_text(intent="deny"),
            ],
            "famille": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_entity(entity="famille"),
                self.from_text(intent="deny"),
            ],
            "connaissance_covid": [
                self.from_entity(entity="connaissance_covid"),
            ],
            "confirm_connaissance_covid": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "lieu_info": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "lieu_info2": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "q13": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q13"),
            ],
            "q14": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q14"),
            ],
            "q15": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q15"),
            ],
            "q16": [
                self.from_text(intent="inform"),
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q16"),
            ],
            "q17": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q17"),
            ],
            "q18": [
                self.from_text(intent="inform"),
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q18"),
            ],
            "q19": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q19"),
            ],
            "q20": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q20"),
            ],
            "q21": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q21"),
            ],
            "q22": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q22"),
            ],
            "q23": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q23"),
            ],
            "q24": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q24"),
            ],
            "q25": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q25"),
            ],
            "q26": [
                self.from_text(intent="inform"),
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q26"),
            ],
            "q27": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q27"),
            ],
            "q28": [
                self.from_text(intent="inform"),
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q28"),
            ],
            "q29": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q29"),
            ],
            "q30": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q30"),
            ],
            "q31": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q31"),
            ],
            "q32": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q32"),
            ],
            "q33": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q33"),
            ],
            "q34": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q34"),
            ],
            "q35": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q35"),
            ],
            "q36": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q36"),
            ],
            "q37": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q37"),
            ],
            "q38": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q38"),
            ],
            "q39": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q39"),
            ],
            "q40": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q40"),
            ],
            "q41": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q41"),
            ],
            "q42": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q42"),
            ],
            "q43": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q43"),
            ],
            "q44": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q44"),
            ],
            "q45": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q45"),
            ],
            "q46": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q46"),
            ],
            "q47": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q47"),
            ],
            "q48": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q48"),
            ],
            "q49": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q49"),
            ],
            "q50": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q50"),
            ],
            "q51": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q51"),
            ],
            "q52": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q52"),
            ],
            "q53": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q53"),
            ],
            "q54": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q54"),
            ],
            "q55": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q55"),
            ],
            "q56": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q56"),
            ],
            "q57": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q57"),
            ],
            "q58": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q58"),
            ],
            "q59": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q59"),
            ],
            "q60": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q60"),
            ],
            "q61": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q61"),
            ],
            "q62": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q62"),
            ],
            "q63": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q63"),
            ],
            "q64": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q64"),
            ],
            "q65": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q65"),
            ],
            "q66": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q66"),
            ],
            "q67": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q67"),
            ],
            "q68": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q68"),
            ],
            "q69": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q69"),
            ],
            "q70": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q70"),
            ],
            "q71": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q71"),
            ],
            "q72": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q72"),
            ],
            "q73": [
                self.from_text(intent="inform"),
                self.from_text(intent="affirm"),
                self.from_intent(intent="info3", value=True),
                self.from_intent(intent="deny", value=False),
                self.from_entity(entity="q73"),
            ],
            "info_covid1": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "info_covid2": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "info_test1": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "info_test2": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "info_test3": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "info_test_remercie": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            "consulter_medecin": [
                self.from_intent(intent="affirm", value=True),
                self.from_intent(intent="deny", value=False),
            ],
            
            "goal": [
                self.from_text(intent="inform"),
            ],
        }

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:

        dispatcher.utter_message("Merci, bon travail!")
        return []

